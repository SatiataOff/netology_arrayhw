var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// выводим список с фамилиями
console.log('Список студентов:');
for (var i = 1; i < studentsAndPoints.length; i+=2) {
  console.log('Студент %s набрал(a) %s баллов', studentsAndPoints[i-1], studentsAndPoints[i]);
}

// выводим максимальный балл
var maxIndex = -1, max, name;
for (var i = 1; i < studentsAndPoints.length; i+=2) {
  if (maxIndex < 0 || studentsAndPoints[i] > max) {
    maxIndex = i;
    max = studentsAndPoints[i];
    name = studentsAndPoints[i-1];
  }
}
console.log('Студент набравший максимальный балл:');
console.log('Студент %s имеет максимальный балл %s', name, max);

// добавить новых студентов
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

// внесение изменений о количестве баллов
var i = studentsAndPoints.indexOf('Антон Павлович');
studentsAndPoints[i+1] += 10;
var j = studentsAndPoints.indexOf('Николай Фролов');
studentsAndPoints[j+1] += 10;

// вывести студентов, не набравших баллов
console.log('Студенты не набравшие баллов:');
for (var i = 1; i < studentsAndPoints.length; i+=2) {
  if (studentsAndPoints[i] == 0) {
    console.log(studentsAndPoints[i-1]);
  }
}

//удаление студентов с 0 баллов

for (var i = studentsAndPoints.length; i >= 0; i--) {
  if (studentsAndPoints[i] === 0) {
    studentsAndPoints.splice(i-1, 2);
  }
}
console.log(studentsAndPoints);
